<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/offline', function () {
    return view('modules/laravelpwa/offline');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/boletos', 'BoletosController@index')->name('boletos.index');
Route::post('/boletos/store','BoletosController@store')->name('boletos.store');
Route::post('/asientos','BoletosController@asientos')->name('boletos.asientos');
Route::get('/formas/pagos', 'BoletosController@formaspagos')->name('formas.pagos');
Route::get('/imprimir/boletos', 'BoletosController@imprimir')->name('boletos.imprimir');



Route::post('paypal','PaymentController@payWithpaypal');
Route:: get('status', 'PaymentCOntroller@getPaymentStatus');
