<?php

namespace App\Http\Controllers;

use App\Boletos;
use App\Compras;
use DB;
use Illuminate\Http\Request;

class BoletosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('boletos');
    }

    public function formaspagos(){
        $datosAsientos=Boletos::all();
        $datosCompra=Compras::all();
        return view('formasPagos',compact('datosCompra','datosAsientos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $boletosA = $request->get('adultos');
        $boletosAdulto = ($boletosA * 70);

        $boletosN = $request->get('ninos');
        $boletosNino = ($boletosN * 50);

        $total = ($boletosNino +  $boletosAdulto);

        DB::table('compras')->insert([
             'adultos' => $request->get('adultos'),
             'ninos' => $request->get('ninos'),
             'asientos' => $request->get('asientos'),
             'total' => $total,
        ]);

        return redirect('/boletos')->with('Mensaje','¡Selecciona tus asientos!');
    }

      public function asientos(Request $request)
    {
        //
    
        $datosAsientos=request()->except('_token');
        Boletos::insert($datosAsientos);
        
        return redirect('/formas/pagos')->with('Mensaje','Selecciona la forma de pago');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Boletos  $boletos
     * @return \Illuminate\Http\Response
     */
    public function show(Boletos $boletos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Boletos  $boletos
     * @return \Illuminate\Http\Response
     */
    public function edit(Boletos $boletos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Boletos  $boletos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Boletos $boletos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Boletos  $boletos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Boletos $boletos)
    {
        //
    }

     public function imprimir()
    {
        $datosAsientos=Boletos::all();
        $datosCompra=Compras::all();
        return view('imprimir',compact('datosCompra','datosAsientos'));
      
    }
}
