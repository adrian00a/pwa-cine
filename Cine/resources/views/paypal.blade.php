<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PayPal</title>
</head>
<body>
    @if($message = Session::get('Success'))
        <div class="card">
            <span onclick="this.parentElement.style.display='none'">&times;</span>
            <p>{{ $message }}</p>
        </div>
        <?php Session::forget('success');?>
    @endif

    @if($message = Session::get('error'))
        <div class="card">
            <span onclick="this.parentElement.style.display='none'">&times;</span>
            <p>{{ $message }}</p>
        </div>
        <?php Session::forget('error');?>
    @endif

    <form class="w3-container w3-display-middle w3-card-4 w3-padding-16" method="POST" id="payment-form" action="{!! URL::to('paypal') !!}">
        <div class="w3-container w3-teal w3-padding-16">Paywith Paypal</div>
        {{ csrf_field() }}
        <h2 class="w3-text-blue">Payment Form</h2>
        <p>Demo Paypal Form In Laravel</p>
        <label class="w3-text-blue"><b>Enter Amount</b></label>
        <input class="w3-input w3-border" id="amount" type="text" name="amount">
        <button class="btn btn-primary">Pay whith Paypal</button>
    </form>


    
    
</body>
</html>