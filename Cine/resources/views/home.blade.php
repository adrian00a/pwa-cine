@extends('layouts.app')

@section('content')
<div class="content">


  

<!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <form method="POST" action="{{route('boletos.store')}}">
          @csrf
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-info" id="exampleModalLongTitle">Selecciona tus boletos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <small>Puedes comprar solo 6 boletos por transacción</small>
            <br/>
            <br/>
            <p class="row justify-content-center">Adultos $70</p>
              <div class="row justify-content-center">
                <input class="form-control col-md-3" type="number" max="3" min="1" name="adultos" placeholder="0">
              </div>
            <br/>
            <p class="row justify-content-center">Niños $50</p>
            <div class="row justify-content-center">
              <input class="form-control col-md-3" type="number" max="3" min="1" name="ninos" placeholder="0">
            </div>
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-success">Aceptar</button>
          </div>
        </div>
        </form>
      </div>
    </div>

    <header class="masthead text-center text-white">
        <div class="masthead-content">
          <div class="container">
            <h2 class="masthead-subheading mb-0">Cartelera para el día {{ $fechaActual }}</h2>
          </div>
        </div>
        <div class="bg-circle-1 bg-circle"></div>
        <div class="bg-circle-2 bg-circle"></div>
        <div class="bg-circle-3 bg-circle"></div>
        <div class="bg-circle-4 bg-circle"></div>
       
      </header>
  
    <br/>
 
      <section >

         <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success text-center" role="alert">
       <h5>{{Session::get('Mensaje')}}</h5>
    </div>
@endif
<!-- Fin -->

        <div class="container card shadow bg-primary">
          <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
              <div class="p-5">
                <img class="img-fluid rounded-circle" src={{asset('Images/02.jpg')}} alt="Bloodshot"/>
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="p-5">
                <h1 class="text-white">Bloodshot</h1>
                <p class="text-white">Murray Ray Garrison es resucitado por un equipo de científicos. Mejorado con nanotecnología, se convierte en una máquina de matar biotecnológica sobrehumana. Cuando Ray entrena por primera vez con otros super soldados, no recuerda nada de su vida anterior.
                <span class="bg-black p-1">124 min</span> <span class="bg-black p-1">B15</span>
                </p>
                <div><h4>Español</h4> 
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                    <time class="ng-scope">14:30</time> 
                  </button>
                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                    <time class="ng-scope">16:10</time>           
                  </button>
                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                    <time class="ng-scope">18:45</time>
                  </button>
                </div>
                <div><h4>Subtitulada</h4> 
                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                  <time class="ng-scope">19:30</time> 
                  </button>
                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModalCenter">
                  <time class="ng-scope">22:10</time>
                  </button>
                </div>
  
              </div>
  
            </div>
          </div>
        </div>
      </section>
  
    <br/>
  
      <section>
       <div class="container card shadow bg-primary">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <div class="p-5">
                <img class="img-fluid rounded-circle" src={{asset('Images/05.jpg')}} alt="Sonic"/>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="p-5">
                <h1 class="text-white">El llamado salvaje</h1>
                <p class="text-white">En el Yukón canadiense en la década de 1890 se está produciendo la fiebre del oro. Los perros de trineo que pueden cubrir el terreno difícil a lo largo del río Klondike son codiciados por muchos y se convierten en un activo valioso. Como resultado, Buck es robado y vendido.
                <span class="bg-black p-1">100 min</span> <span class="bg-black p-1">A</span>
                </p>
  
                <div><h4>Español</h4> 
                  <time class="btn btn-light ng-scope">14:30</time> 
                  <time class="btn btn-light ng-scope">16:10</time>
                  <time class="btn btn-light ng-scope">18:45</time>
                </div>
                <div><h4>Subtitulada</h4> 
                  <time class="btn btn-light ng-scope">19:30</time> 
                  <time class="btn btn-light ng-scope">22:10</time>
                </div>
  
              </div>
            </div>
          </div>
        </div>
      </section>
  
    <br/>
  
      <section>
        <div class="container card shadow bg-primary">
          <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
              <div class="p-5">
                <img class="img-fluid rounded-circle" src={{asset('Images/04.jpg')}} alt="Aves de Presa"/>
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="p-5">
                <h1 class=" text-white">Aves de presa</h1>
                <p class="text-white">Después de separarse de Joker, Harley Quinn y otras tres heroínas, Canario Negro, Cazadora y Renée Montoya, unen sus fuerzas para salvar a una niña del malvado rey del crimen Máscara Negra.
                <span class="bg-black p-1">109 min</span> <span class="bg-black p-1">B15</span>
                </p>
  
                 <div><h4>Español</h4> 
                  <time class="btn btn-light ng-scope">14:30</time> 
                  <time class="btn btn-light ng-scope">16:10</time>
                  <time class="btn btn-light ng-scope">18:45</time>
                </div>
                <div><h4>Subtitulada</h4> 
                  <time class="btn btn-light ng-scope">19:30</time> 
                  <time class="btn btn-light ng-scope">22:10</time>
                </div>
  
              </div>
            </div>
          </div>
        </div>
      </section>
  
    <br/>

  
    <br/>
  
      <!-- Footer -->
      <footer class="py-5 bg-black">
        <div class="container">
          <p class="m-0 text-center text-white small"> ITMorelia Adrian L-V</p>
        </div>
        <!-- /.container -->
      </footer>
</div>
@endsection
