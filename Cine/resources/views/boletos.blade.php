@extends('layouts.app')
<br/>
<br/>
<br/>
@section('content')

 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-primary text-center" role="alert">
       <h5>{{Session::get('Mensaje')}}</h5>
    </div>
@endif
<!-- Fin -->

<br/>
<br/>
<div class="row justify-content-center">
<div class="card shadow" style="width:60%">
<div class="card-body">
    <h5 class="card-header bg-info text-white text-center">PANTALLA</h5>
    <br/>
     
<form method="POST" action="{{route('boletos.asientos')}}">
          @csrf
    <span class="row justify-content-center">
       <!-- Default inline 1-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline1" value="1" name="asientos">
                <label class="custom-control-label" for="defaultInline1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1</label>
            </div>
  
        <!-- Default inline 2-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline2" value="2" name="asientos">
                <label class="custom-control-label" for="defaultInline2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2</label>
            </div>
  
        <!-- Default inline 3-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline3" value="3" name="asientos">
                <label class="custom-control-label" for="defaultInline3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3</label>
            </div>

    </span>
    <span class="row justify-content-center">
              <!-- Default inline 1-->
              <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline4" value="4" name="asientos">
                <label class="custom-control-label" for="defaultInline4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4</label>
            </div>
  
        <!-- Default inline 2-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline5" value="5" name="asientos">
                <label class="custom-control-label" for="defaultInline5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5</label>
            </div>
  
        <!-- Default inline 3-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline6" value="6" name="asientos">
                <label class="custom-control-label" for="defaultInline6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6</label>
            </div>
    </span>
    <span class="row justify-content-center">
              <!-- Default inline 1-->
              <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline7" value="7" name="asientos">
                <label class="custom-control-label" for="defaultInline7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7</label>
            </div>
  
        <!-- Default inline 2-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline8" value="8" name="asientos">
                <label class="custom-control-label" for="defaultInline8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8</label>
            </div>
  
        <!-- Default inline 3-->
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" class="custom-control-input" id="defaultInline9" value="9" name="asientos">
                <label class="custom-control-label" for="defaultInline9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9</label>
            </div>
    </span>
    <br/>
        <div class="row justify-content-center">
            <a href="{{ route('home')}}" class="btn btn-danger">Cancelar</a>&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-success">Pagar</button>
            
        </div>

   
</div>
</div>
</div>
</form>
@endsection