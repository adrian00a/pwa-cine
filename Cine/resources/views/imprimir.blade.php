@extends('layouts.app')
<br/>
<br/>
<br/>
<br/>
<br/>
@section('content')

 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-success text-center" role="alert">
       <h5>{{Session::get('Mensaje')}}</h5>
    </div>
@endif
<!-- Fin -->


<div class="container col-sm-7  ">
<div class="card bg-light" style="padding:10px" >
 <div class="panel-heading ">
     
      <div class="row ">
              <div class="col-md-12">
                   <img src={{asset('Images/logo.png')}} alt="Logo" width=15% />
              </div>

       </div>
       <h4 class="d-flex justify-content-center">Boletos</h4>
       <br/> 
     <div class="row justify-content-center ">
     	@foreach($datosCompra as $item)
              <div class="col-md-2 col-sm-2 col-xs-2 ">
                  <span class="help-block text-muted small-font" >Pelicula</span>
                  <input type="text" class="form-control" value="{{ $item->pelicula }}" disabled />
              </div>
         @endforeach
         @foreach($datosAsientos as $item)
         <div class="col-md-2 col-sm-2 col-xs-2">
                  <span class="help-block text-muted small-font" >Asientos</span>
                  <input type="text" class="form-control" value="{{ $item->asientos }}" disabled />
              </div>
          @endforeach
        <div class="col-md-2 col-sm-2 col-xs-2">
                  <span class="help-block text-muted small-font" >Hora</span>
                  <input type="text" class="form-control" placeholder="18:45" disabled />
              </div>
                <div class="col-md-1 col-sm-1 col-xs-1">
                  <span class="help-block text-muted small-font" >Sala</span>
                  <input type="text" class="form-control" placeholder="2" disabled />
              </div>
         <div class="col-md-3 col-sm-3 col-xs-3">
<img src={{asset('Images/qr.png')}} alt="Logo" width=70% />
         </div>
          </div>
    
     
                   </div>
              </div>
<br/> 
<br/>   



<script type="text/javascript">
function imprimir()
{
  var Obj = document.getElementById("ocultar");
  Obj.style.visibility = 'hidden';
  window.print();
}
</script>



<div class="d-flex justify-content-center ">
      <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
            <input type="button" id="ocultar" onClick="imprimir()" value="Imprimir" class="btn btn-warning btn-block rounded-pill mt-5" />
       </div>
</div>
    </div>
 @endsection