@extends('layouts.app')

@section('content')

   
                  <script>
                    Push.create("Hello world!");
                  </script>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> <img src={{asset('Images/logo.png')}} alt="Logo" width=35% /></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Inicia sesión o <a href="{{route('register')}}">registrate</a> para poder comprar tus boletos.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="{{ route('login') }}" class="btn btn-primary">Iniciar sesión</a>
      </div>
    </div>
  </div>
</div>


                    <header class="masthead text-center text-white">
                      <div class="masthead-content">
                        <div class="container">
                            <img class="rounded-circle" src={{asset('Images/01.jpg')}} alt="01" width="60%"/>
                          <h3 class="masthead-subheading mb-0">¡CineCAT!</h3>
                          
                        </div>
                      </div>
                      <div class="bg-circle-1 bg-circle"></div>
                      <div class="bg-circle-2 bg-circle"></div>
                      <div class="bg-circle-3 bg-circle"></div>
                      <div class="bg-circle-4 bg-circle"></div>
                    </header>
                
                    <section>
                      <div class="container">
                        <div class="row align-items-center">
                          <div class="col-lg-6 order-lg-2">
                            <div class="p-5">
                              <img class="img-fluid rounded-circle" src={{asset('Images/02.jpg')}} alt="Bloodshot"/>
                            </div>
                          </div>
                          <div class="col-lg-6 order-lg-1">
                            <div class="p-5">
                            
                      
                          
                              <label class="text-secondary d-flex justify-content-center "><h3>Bloodshot</h3></label>
                     
                              <p>Murray Ray Garrison es resucitado por un equipo de científicos. Mejorado con nanotecnología, se convierte en una máquina de matar biotecnológica sobrehumana. Cuando Ray entrena por primera vez con otros super soldados, no recuerda nada de su vida anterior.</p>
                              <span class="bg-black p-1 text-white">124 min</span> <span class="bg-black p-1 text-white">B15</span>
                              </p>
                                <div><h4>Español</h4> 
                                  <!-- Button trigger modal -->
                                   <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                       <time class="ng-scope">14:30</time> 
                                  </button>
                                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                       <time class="ng-scope">16:10</time> 
                                  </button>
                                  <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                       <time class="ng-scope">18:45</time> 
                                  </button>
                                </div>
                                <div><h4>Subtitulada</h4> 
                                     <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                       <time class="ng-scope">19:30</time> 
                                  </button>
                                     <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                       <time class="ng-scope">22:10</time> 
                                  </button>
                                </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                
                    <section>
                      <div class="container">
                        <div class="row align-items-center">
                          <div class="col-lg-6">
                            <div class="p-5">
                                <img class="img-fluid rounded-circle" src={{asset('Images/03.jpg')}} alt="Sonic"/>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="p-5">
                              <label class="text-secondary d-flex justify-content-center "><h3>Sonic</h3></label>
                              <p>Sonic intenta navegar por las complejidades de la vida en la Tierra con su nuevo mejor amigo, un humano llamado Tom Wachowski. Pronto deben unir fuerzas para evitar que el malvado Dr. Robotnik capture a Sonic y use sus poderes para dominar el mundo.</p>
                              <span class="bg-black p-1 text-white">100 min</span> <span class="bg-black p-1 text-white">A</span>
                                  </p>
                    
                                  <div><h4>Español</h4>
                                       <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                            <time class="ng-scope">14:30</time> 
                                      </button> 
                                      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                            <time class="ng-scope">16:10</time>
                                      </button> 
                                      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                            <time class="ng-scope">18:45</time>
                                      </button> 
                                  </div>

                                  <div><h4>Subtitulada</h4> 
                                     <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                            <time class="ng-scope">19:30</time> 
                                      </button>
                                      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                         <time class="ng-scope">22:10</time> 
                                      </button>                                  
                                  </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                
                    <section>
                      <div class="container">
                        <div class="row align-items-center">
                          <div class="col-lg-6 order-lg-2">
                            <div class="p-5">
                                <img class="img-fluid rounded-circle" src={{asset('Images/04.jpg')}} alt="Aves de Presa"/>
                            </div>
                          </div>
                          <div class="col-lg-6 order-lg-1">
                            <div class="p-5">
                              <label class="text-secondary d-flex justify-content-center "><h3>Aves de presa</h3></label>
                              <p>Después de separarse de Joker, Harley Quinn y otras tres heroínas, Canario Negro, Cazadora y Renée Montoya, unen sus fuerzas para salvar a una niña del malvado rey del crimen Máscara Negra.</p>
                              <span class="bg-black p-1 text-white">109 min</span> <span class="bg-black p-1 text-white">B15</span>
                              </p>
                
                                   <div><h4>Español</h4>
                                     <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                         <time class=" ng-scope">14:30</time> 
                                      </button>  
                                      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                          <time class="ng-scope">16:10</time>
                                      </button>  
                                  </div>
                                  <div><h4>Subtitulada</h4> 
                                      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#exampleModal">
                                          <time class="ng-scope">19:30</time> 
                                      </button>
                                   
                                
                                  </div>
  
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                
                    <!-- Footer -->
                    <footer class="py-5 bg-black">
                      <div class="container">
                        <p class="m-0 text-center text-white small">ITMorelia Adrian L-V</p>
                      </div>
                    <!-- /.container -->
                     
                    </footer>

                  
                  <script>
                    Push.create("Hello world!");
                  </script>
                 

 @endsection