 
@extends('layouts.app')

@section('content')
<br/>
<br/>
<br/>
<br/>

 <!-- Muestro mensaje de exito -->
@if(Session::has('Mensaje'))
    <div class="alert alert-primary text-center" role="alert">
       <h5>{{Session::get('Mensaje')}}</h5>
    </div>
@endif
<!-- Fin -->


<div class="row justify-content-center">
        <div class="col-md-10 shadow p-2">
            <div class="card">
               <div class="card-header text-white bg-info"><h4><strong>{{ __('Formas de pago') }}</strong></h4></div>

                <div class="card-body">
                    <!-- Button trigger modal -->
                        <button type="button" class="btn btn-light" data-toggle="modal" data-target="#exampleModalCenter">
                           <img src={{asset('Images/paypal.png')}} alt="Logo" width=15% />
                        </button>
                     
                </div>
            </div>
        </div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-secondary" id="exampleModalLongTitle">Datos de la compra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

  <form method="POST" action="{!! URL::to('paypal') !!}">
                        @csrf
                        @foreach($datosCompra as $item)

                        <ul>
                         <h5 class="text-secondary">Pelicula</h5>
                            <strong><li>Bloodshot</li></strong>
                         <h5 class="text-secondary">Hora</h5>
                            <strong><li>18:45</li></strong>
                         <h5 class="text-secondary">Boletos Adultos</h5>  
                            <strong><li>{{ $item->adultos }}</li></strong>
                         <h5 class="text-secondary">Boletos Niños</h5>   
                            <strong><li>{{ $item->ninos }}</li></strong>
                        </ul>
                         @endforeach
                       @foreach($datosAsientos as $item)
                        <ul>
                           <h5 class="text-secondary">Asientos</h5> 
                              <strong><li>{{  $item->adultos }} </li></strong> 
                        </ul>
                        @endforeach
                        @foreach($datosCompra as $item)
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Total') }}</label>

                            <div class="col-md-6">
                                <input type="text" value="{{$item->total}}" class="form-control bg-dark text-white" name="amount">

              
                            </div>
                        </div>
                     @endforeach
     
        
               <div class="modal-footer">
                   <a href="{{ route('home') }}" class="btn btn-secondary" >
                     Cancelar
                    </a>
                     <button type="submit" class="btn btn-primary">Aceptar</button>
              </div>
          </form>

      </div>
    </div>
  </div>
</div>



  @endsection
